### What is this repository for? ###

* This Joomla 3 component displays a list of users and all their custom fields created using [MProfile](http://malkesh.com/joomla-extensions/item/38-mprofile.html).

* To install: Zip com_MProfileUserList and upload it in the Joomla administrator interface

### Does it work in Joomla 2.5? ###
No. I've commented the code with Joomla 2.5 in a few places where the methods have been deprecated.  Been after changing these methods it still doesn't work in 2.5.  If anybody knows how to get it to work please let me know.

### Created using this tutorial ###

* Biswareup Adhikari Joomla 2.5 Customer Development

Components made from this tutorial: https://www.youtube.com/watch?v=dVBnlL3ZQ4A