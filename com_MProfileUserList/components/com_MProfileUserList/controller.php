<?php

defined('_JEXEC') or die("Access denied");
jimport('joomla.application.component.controller');

class UserListController extends JControllerLegacy // for Joomla 2.5 change to JController
{
  
  function display($cachable = false, $urlparams = false) // for Joomla 2.5 remove the parameters
  {
    //$doc = JFactory::getDocument();
    //$doc -> addStyleSheet( JURI::base().'media/com_userlist/css/frontend.css'    );
    //$doc -> addScript( JURI::base().'media/com_userlist/js/frontend.js'    );
    
		$usersWithFields = $this -> getUsersWithFieldsFromDatabase();
		echo '<h1 class="MProfileUserList">Users</h1>';		
		if (count($usersWithFields) == 0 )
			return;		
    echo '<table class="MProfileUserList">';
		echo '<tr class="MProfileUserList">';		
		foreach ($usersWithFields[0] as $name => $value)
			echo '<th class="MProfileUserList">' . $name . '</th>';
		echo '</tr>';		
    foreach ($usersWithFields as $user)
    {
      echo '<tr class="MProfileUserList">';
			foreach ($user as $name => $value)
				echo '<td class="MProfileUserList">' . $value . '</td>';
      echo '</tr>';
    }
    echo '</table>';
  }
  
  function getUsersWithFieldsFromDatabase()
  {
		$fields = $this -> getMProfileFieldsFromDatabase();
		$users = $this -> getUsersFromDatabase();
		$fieldnames = self :: getFieldNames($fields);
		$usersWithFields = self :: combineUsersAndFields($users, $fields, $fieldnames);
//	echo '<pre>'; print_r ($usersWithFields); echo '</pre>';
		return $usersWithFields;    
  }
	
	static function combineUsersAndFields(&$users, &$fields, &$fieldnames)
	{
		$usersWithFields = array();
		foreach ($users as $user)
		{
			$newUser = self :: makeNewUserWithEmptyFieldNames($user, $fieldnames);
			foreach ($fields as $field)
			{
				if ($field -> user_id != $user -> id)
					continue;
				$newUser[$field -> name] = $field -> value;			
			}
			$usersWithFields[] = $newUser;
		}
		return $usersWithFields;    
	}
	
	static function makeNewUserWithEmptyFieldNames(&$user, &$fieldnames)
	{
		$userWithEmptyFieldNames = array
		(
			'Id' => $user -> id,
			'Name'	=> $user -> name,
			'Username'	=> $user -> username,
			'Email'	=> $user -> email
		);
		foreach ($fieldnames as $field)
			$userWithEmptyFieldNames[$field] = '';		
		return $userWithEmptyFieldNames;
	}
	
	static function getFieldNames(&$fields)
	{
		$fieldNames = array();
		foreach ($fields as $field)
			if (! in_array($field -> name, $fieldNames))
				$fieldNames[] = $field -> name;
		return $fieldNames;
	}
	
	function getUsersFromDatabase()
	{
		$db = JFactory::getDBO();	
    $query = $db -> getQuery(true);	    
    $query
        ->select($db->quoteName(array('u.id', 'u.name', 'u.username', 'u.email')))
        ->from($db->quoteName('#__users', 'u'))        
        ->order($db->quoteName('u.name') . ' ASC');		
    $db ->setQuery($query);
		$result = $db -> loadObjectList();	
    return $result;    		
	}
	
	function getMProfileFieldsFromDatabase()
	{
		$db = JFactory::getDBO();	
    $query = $db -> getQuery(true);	    
    $query
        ->select($db->quoteName(array('v.user_id', 'v.field_id', 'v.value', 'f.name')))
        ->from($db->quoteName('#__mprofile', 'f'))  //fields
        ->join('INNER', $db->quoteName('#__mprofile_values', 'v') . ' ON (' . $db->quoteName('v.field_id') . ' = ' . $db->quoteName('f.id') . ')') //values        
				->where($db->quoteName('f.published') . ' = \'1\'')
        ->order($db->quoteName('v.user_id') . ' ASC');		
    $db ->setQuery($query);
		$result = $db -> loadObjectList();	
    return $result;    		
	}
  
  //function delete()
  //{
  //  $app=JFactory::getApplication();
  //  $id = JRequest::getVar('id');
  //  echo "You want to delete $id";
  //  $app->close();  
  //}
  
}